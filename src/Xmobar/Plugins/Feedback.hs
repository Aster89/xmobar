{-# LANGUAGE FlexibleContexts #-}

module Xmobar.Plugins.Feedback (Feedback(..)) where

import Control.Concurrent.Async (withAsync)
import Control.Exception (finally)
import Control.Monad (forever, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans (MonadIO)
import Control.Monad.Trans.State.Strict (evalStateT, get, modifyM)
import Data.IORef (atomicModifyIORef', IORef, newIORef, readIORef)
import Data.Maybe (isJust)
import System.Directory (removeFile)
import System.Exit (ExitCode(..))
import System.Process (readProcessWithExitCode)
import Xmobar.Run.Exec (Exec(..), tenthSeconds)

data Feedback = Feedback deriving (Show, Read)

instance Exec Feedback where
  alias Feedback = "feedback"
  start Feedback cb = do
    ref <- newIORef Nothing
    pipe <- makePipe
    withAsync (forever $ pipe `blockingReadInto` ref)
              (const (forever (do liftIO (tenthSeconds 1)
                                  button <- liftIO $ readFromAndClear ref
                                  modifyM (`evolveWith` button)
                                  s <- get
                                  liftIO $ cb $ concatMap (\(i, s') -> enableClick pipe i s') (enum s))
                              `evalStateT` initialState))
      `finally` removeFile pipe
      where
        enum = zip [0..]

initialState :: [String]
initialState = replicate 5 "█" ++ replicate 5 "░"

evolveWith :: (MonadIO m) => [String] -> Maybe (Int, MouseButton) -> m [String]
evolveWith ss Nothing = return ss
evolveWith ss (Just (_, B2)) = return ss
evolveWith ss (Just (_, B3)) = return ss
evolveWith ss@(s:s':_) (Just (i, B1)) = return $
  if i == 0 && s == "█" && s' == "░"
     then replicate (length ss) "░"
     else replicate (i + 1) "█"
       ++ replicate (length ss - i - 1) "░"
evolveWith ss (Just (_, b)) = return $
  let (full, shaded) = break (== "░") ss
  in case b of
    B4 -> if null shaded then ss else "█":full ++ tail shaded
    B5 -> if null full then ss else tail full ++ "░":shaded
    _ -> error "bla"

enableClick :: String -> Int -> String -> String
enableClick fifo i k =
             "<action=`echo '(" ++ show i ++ "," ++ show B1 ++ ")' > " ++ fifo ++ "` button=" ++ toIntStr B1 ++ ">"
          ++ "<action=`echo '(" ++ show i ++ "," ++ show B2 ++ ")' > " ++ fifo ++ "` button=" ++ toIntStr B2 ++ ">"
          ++ "<action=`echo '(" ++ show i ++ "," ++ show B3 ++ ")' > " ++ fifo ++ "` button=" ++ toIntStr B3 ++ ">"
          ++ "<action=`echo '(" ++ show i ++ "," ++ show B4 ++ ")' > " ++ fifo ++ "` button=" ++ toIntStr B4 ++ ">"
          ++ "<action=`echo '(" ++ show i ++ "," ++ show B5 ++ ")' > " ++ fifo ++ "` button=" ++ toIntStr B5 ++ ">"
          ++ k
          ++ "</action>"
          ++ "</action>"
          ++ "</action>"
          ++ "</action>"
          ++ "</action>"

data MouseButton = B1 -- : left click
                 | B2 -- : middle click
                 | B3 -- : right click
                 | B4 -- : scroll up
                 | B5 -- : scroll down
                 deriving (Read, Show)

toIntStr :: MouseButton -> String
toIntStr B1 = show (1 :: Int)
toIntStr B2 = show (2 :: Int)
toIntStr B3 = show (3 :: Int)
toIntStr B4 = show (4 :: Int)
toIntStr B5 = show (5 :: Int)

removeLinebreak :: [a] -> [a]
removeLinebreak = init

makePipe :: IO String
makePipe = do
    (_, n, _) <- readProcessWithExitCode "uuidgen" [] ""
    let pipe = "/tmp/feedback-" ++ removeLinebreak n
    (_, _, _) <- readProcessWithExitCode "mkfifo" [pipe] ""
    return pipe

blockingReadInto :: String -> IORef (Maybe (Int, MouseButton)) -> IO ()
file `blockingReadInto` clickedButton = do
  (ret, out, _) <- readProcessWithExitCode "cat" [file] ""
  case ret of
    ExitSuccess -> atomicModifyIORef' clickedButton (const (Just $ read $ removeLinebreak out, ()))
    ExitFailure _ -> error "how is this possible?"


readFromAndClear :: IORef (Maybe a) -> IO (Maybe a)
readFromAndClear clickedButton = do
  clicked' <- readIORef clickedButton
  when (isJust clicked')
       (clear clickedButton)
  return clicked'
  where
    clear = (`atomicModifyIORef'` const (Nothing, ()))
